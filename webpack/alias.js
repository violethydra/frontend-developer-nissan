const path = require('path');
const PATHS = require('./source');

module.exports = {
  '~': PATHS.src,
  '~modules': path.join(PATHS.src, 'modules'),
  '~/*': PATHS.src,
  '~modules/*': path.join(PATHS.src, 'modules'),
};
