const PATHS = require('./source');

module.exports = {
  historyApiFallback: true,
  open: false,
  host: 'localhost',
  port: 3000,
  allowedHosts: ['auto'],
  watchFiles: PATHS.watchFiles,
  hot: true,
  static: true,
  https: false,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
  },
};
