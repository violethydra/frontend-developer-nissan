const path = require('path');

const root = process.cwd();
const SRC = path.resolve(root, 'src');
module.exports = {
  src: path.resolve(root, 'src'),
  dist: path.resolve(root, 'public'),
  assets: path.resolve(root, 'assets'),
  icon: path.resolve(root, 'favicon.ico'),
  svgo: path.resolve(root, 'svgo.config.js'),
  template: path.join(SRC, 'index.pug'),
  watchFiles: path.join(SRC, '**/*.pug'),
};
