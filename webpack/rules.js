const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PATHS = require('./source');

module.exports = [
  {
    test: /\.html$/,
    loader: 'html-loader',
  },

  {
    test: /\.(eot|ttf|woff|woff2|png|jpg|gif)$/i,
    type: 'asset/resource',
  },

  {
    test: /\.pug$/i,
    use: ['simple-pug-loader'],
  },

  {
    test: /\.(pc|c)ss$/i,
    use: [
      {
        loader: MiniCssExtractPlugin.loader,
      },

      {
        loader: 'css-loader',
        options: { modules: false },
      },
      'postcss-loader',
    ],
  },
  {
    test: /\.(ts|tsx)$/i,
    use: ['babel-loader', 'ts-loader'],
    exclude: ['/node_modules/'],
  },

  {
    test: /\.svg$/,
    type: 'asset/resource',
    use: [
      {
        loader: 'svgo-loader?limit=8192',
        options: {
          configFile: PATHS.svgo,
        },
      },
    ],
  },
];
