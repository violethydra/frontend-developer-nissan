// const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPugPlugin = require('html-webpack-pug-plugin');
const ImageMinimizerPlugin = require('image-minimizer-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('@soda/friendly-errors-webpack-plugin');

const TerserPlugin = require('terser-webpack-plugin');

const nameENV = process.env.NODE_ENV;
const isProduction = process.env.NODE_ENV === 'production';
console.log(`[LOG] isProduction`, `<${typeof isProduction}>`, isProduction);

const devServer = require('./webpack/server');
const PATHS = require('./webpack/source');
const alias = require('./webpack/alias');
const rules = require('./webpack/rules');

const _clearImg = isProduction
  ? [
      new ImageMinimizerPlugin({
        minimizerOptions: {
          plugins: [
            ['gifsicle', { interlaced: true }],
            ['jpegtran', { progressive: true }],
            ['optipng', { optimizationLevel: 5 }],
          ],
        },
      }),
    ]
  : [];

const config = {
  experiments: {
    asset: true,
  },

  mode: isProduction ? 'production' : 'development',
  entry: PATHS.src,
  output: {
    filename: '[name].bundle.js',
    path: PATHS.dist,

    assetModuleFilename: 'assets/[hash][ext][query]',

    sourceMapFilename: '.map/[file].map[query]',
    hotUpdateChunkFilename: '.hot/[id].[fullhash:5].hot-update.js',
    hotUpdateMainFilename: '.hot/[fullhash:5].hot-update.json',
  },

  target: 'web',

  devtool: isProduction ? false : 'inline-source-map',
  devServer,
  plugins: [
    new CleanWebpackPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    // new FaviconsWebpackPlugin({ logo: PATHS.icon }),

    new HtmlWebpackPlugin({
      title: nameENV,
      template: PATHS.template,
      filename: 'index.html',
      minify: !!isProduction,
    }),

    new HtmlWebpackPugPlugin({ adjustIndent: true }),
    new MiniCssExtractPlugin({
      filename: '[name].bundle.css',
      chunkFilename: '[id].css',
      ignoreOrder: false,
    }),
    // ..._clearImg,
  ],
  optimization: {
    minimize: !!isProduction,
    minimizer: isProduction ? [new TerserPlugin({ parallel: true })] : [],

    splitChunks: {
      cacheGroups: {
        vendor: {
          name: 'vendors',
          test: /node_modules/,
          chunks: 'all',
          enforce: true,
        },
        styles: {
          name: 'styles',
          type: 'css/mini-extract',
          test: /custom\.s?css$/,
          chunks: 'all',
          enforce: true,
        },
      },
    },
  },

  module: {
    rules,
  },
  resolve: {
    alias,
    extensions: ['.tsx', '.ts', '.js'],
  },
};

module.exports = () => {
  if (isProduction) config.mode = 'production';
  else config.mode = 'development';
  return config;
};
