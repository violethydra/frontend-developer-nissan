const { resolve } = require('path');

const rootNoExt = ({ name, type }) => resolve(process.cwd(), `src/${type}/`, `${name}`);

const lib = {
  'postcss-at-rules-variables': {},
  'postcss-nested': {},
  'postcss-inline-svg': {},

  'postcss-mixins': {
    beforeEach: [],
    afterEach: ['postcss-at-rules-variables', 'postcss-nested'],
    mixinsDir: rootNoExt({ name: 'mixins', type: 'pcss' }),
  },

  'postcss-for': {},
  'postcss-each': {
    beforeEach: [],
    afterEach: ['postcss-at-rules-variables', 'postcss-nested'],
  },

  'postcss-url': {},
  'postcss-import': {},

  'postcss-responsive-type': {},
  'postcss-hexrgba': {},

  'postcss-sorting': {
    'properties-order': [
      'display',
      'width',
      'height',
      'margin',
      'padding',
      'border',
      'background',
      'position',
      'top',
      'right',
      'bottom',
      'left',
      'transform',
      //
    ],
  },
  'postcss-pxtorem': {
    rootValue: 16,
    propList: ['*', '!border*'],
  },
  'postcss-calc': {},
  'postcss-gap-properties': {},
  'postcss-combine-duplicated-selectors': {},
};
module.exports = {
  plugins: {
    ...lib,
    cssnano: {
      preset: [
        'default',
        {
          svgo: false,
          cssDeclarationSorter: false,
          autoprefixer: false,
          calc: false,
          discardComments: { removeAll: true },
        },
      ],
    },
  },
  order: 'presetEnvAndCssnanoLast',
  preset: {
    stage: 2,
    autoprefixer: {
      grid: true,
      flex: true,
      overrideBrowserslist: ['last 3 versions'],
    },
  },
};
