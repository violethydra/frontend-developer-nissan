//reset
import 'normalize.css/normalize.css';

// system
import '~/fonts/index.pcss';
import '~/pcss/var.pcss';
import '~/pcss/index.pcss';
import '~/pcss/custom.pcss';

// modules
import '~modules/card/index.pcss';
import '~modules/hero/index.pcss';
import '~modules/slider/index.pcss';
import '~modules/hero/index.pcss';

import { hookSlider } from '~/modules/slider';
hookSlider();
