import Glide from '@glidejs/glide';

import '@glidejs/glide/dist/css/glide.core.css';
import '@glidejs/glide/dist/css/glide.theme.css';

export const hookSlider = () => {
  const glob_test = true;

  if (document.querySelector('[data-js="sliderRun"]')) {
    const init = new Glide('[data-js="sliderRun"]', {
      type: 'slider',
      perView: 4,
      rewind: false,
      bound: true,
      dragThreshold: 75,
      swipeThreshold: 75,
      throttle: 10,
      startAt: 0,
      breakpoints: {
        550: {
          perView: 1,
        },
        850: {
          perView: 2,
        },
        1247: {
          perView: 3,
        },
      },
      // peek: { before: 0, after: 0 },
    });

    init.mount();
  }
};
